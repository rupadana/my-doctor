import IconBackDark from './arrow_back_24px.svg';
import IconAddPhoto from './btn_add_photo.svg';
import IconRemovePhoto from './btn_remove_photo.svg';
import IconDoctor from './ic-doctor.svg';
import IconDoctorActive from './ic-doctor-active.svg';
import IconMessages from './ic-messages.svg';
import IconMessagesActive from './ic-messages-active.svg';
import IconHospitals from './ic-hospitals.svg';
import IconHospitalsActive from './ic-hospitals-active.svg';
import IconStar from './ic-star.svg';
import IconBackLight from './ic-back-light.svg';
import IconNext from './ic-next.svg';
import IconSendDark from './ic-send-dark.svg';
import IconSendLight from './ic-send-light.svg';

export {
  IconBackDark,
  IconAddPhoto,
  IconRemovePhoto,
  IconDoctor,
  IconDoctorActive,
  IconMessages,
  IconMessagesActive,
  IconHospitals,
  IconHospitalsActive,
  IconStar,
  IconBackLight,
  IconNext,
  IconSendDark,
  IconSendLight,
};
