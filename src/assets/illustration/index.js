import ILLogo from './logo.svg';
import ILGetStarted from './get-started.png';
import ILNullPhoto from './user_photo_null.png';
import ILCatUmum from './icon_doctor_umum.svg';
import ILCatPsikiater from './icon_dokter_psikiater.svg';
import ILCatObat from './icon_dokter_obat.svg';
import ILHospitalBG from './hospitals-background.png';

export {
  ILLogo,
  ILGetStarted,
  ILNullPhoto,
  ILCatObat,
  ILCatUmum,
  ILCatPsikiater,
  ILHospitalBG,
};
