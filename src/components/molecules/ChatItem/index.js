import React from 'react';
import IsMe from './IsMe';
import Other from './other';
const ChatItem = ({isMe}) => {
  if (isMe) {
    return <IsMe />;
  }
  return <Other />;
};

export default ChatItem;
